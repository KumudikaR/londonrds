module.exports = function() {
    this.dbConnections = [];

    this.dbConnections["d"] = {
        host: process.env.EndPoint_rdsD,
        port: process.env.Port_rdsD,
        user: process.env.User_rdsD,
        password: process.env.Password_rdsD,
        database: "j"
    };
};